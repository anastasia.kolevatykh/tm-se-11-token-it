package ru.kolevatykh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.Project;
import ru.kolevatykh.tm.util.ConsoleInputUtil;

public final class ProjectUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-update";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "pu";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update selected project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        System.out.println("[PROJECT UPDATE]\nEnter project id: ");
        @NotNull final String id = ConsoleInputUtil.getConsoleInput();
        if (id.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        @Nullable final Project project = serviceLocator.getProjectEndpoint().findProjectById(token, id);
        if (project == null) {
            throw new Exception("[The project '" + id + "' does not exist!]");
        }

        System.out.println("Enter new project name: ");
        @NotNull final String name = ConsoleInputUtil.getConsoleInput();

        System.out.println("Enter new project description: ");
        @NotNull final String description = ConsoleInputUtil.getConsoleInput();

        System.out.println("Enter project start date: ");
        @NotNull final String startDate = ConsoleInputUtil.getConsoleInput();

        System.out.println("Enter project end date: ");
        @NotNull final String endDate = ConsoleInputUtil.getConsoleInput();

        serviceLocator.getProjectEndpoint().mergeProject(token, id, name, description, startDate, endDate);
        System.out.println("[OK]");
    }
}
