package ru.kolevatykh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.Exception_Exception;

public final class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "pcl";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tRemove all projects.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception_Exception {
        @Nullable final String token = serviceLocator.getToken();
        System.out.println("[PROJECT CLEAR]");
        serviceLocator.getTaskEndpoint().removeTasksWithProjectId(token);
        serviceLocator.getProjectEndpoint().removeAllProjects(token);
        System.out.println("[Removed all projects with tasks.]\n[OK]");
    }
}
