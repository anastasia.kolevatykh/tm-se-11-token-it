package ru.kolevatykh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.Project;
import ru.kolevatykh.tm.util.ConsoleInputUtil;
import ru.kolevatykh.tm.wrapper.ProjectWrapper;
import java.util.List;

public final class ProjectSearchCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-search";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ps";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tShow all projects that contains a word or phrase.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        System.out.println("[PROJECT SEARCH]\nEnter project search: ");
        @NotNull final String search = ConsoleInputUtil.getConsoleInput();
        if (search.isEmpty()) {
            throw new Exception("[The id can't be empty.]");
        }

        @Nullable final List<Project> projectList = serviceLocator.getProjectEndpoint().findProjectsBySearch(token, search);
        if (projectList == null) {
            throw new Exception("[No projects yet.]");
        }

        System.out.println("[PROJECT LIST]");
        @NotNull final StringBuilder projects = new StringBuilder();
        int i = 0;

        for (@NotNull final Project project : projectList) {
            ProjectWrapper projectWrapper = new ProjectWrapper(project);
            projects
                    .append(++i)
                    .append(". ")
                    .append(projectWrapper.toString())
                    .append(System.lineSeparator());
        }

        @NotNull final String projectString = projects.toString();
        System.out.println(projectString);
    }
}
