package ru.kolevatykh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.*;
import ru.kolevatykh.tm.util.ConsoleInputUtil;
import java.lang.Exception;

public final class TaskUpdateStatusCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-update-status";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "tus";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tUpdate selected task status.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        System.out.println("[TASK UPDATE STATUS]\nEnter task id: ");
        @NotNull final String id = ConsoleInputUtil.getConsoleInput();
        if (id.isEmpty()) {
            throw new Exception("[The name can't be empty.]");
        }

        @Nullable final Task task = serviceLocator.getTaskEndpoint().findTaskById(token, id);
        if (task == null) {
            throw new Exception("[The task '" + id + "' does not exist!]");
        }

        System.out.println("Enter new STATUS name: PLANNED INPROCESS READY");
        @NotNull final String status = ConsoleInputUtil.getConsoleInput();
        if (status.isEmpty()) {
            throw new Exception("[The status can't be empty.]");
        }

        serviceLocator.getTaskEndpoint().mergeTaskStatus(token, id, status);
        System.out.println("[OK]");
    }
}
