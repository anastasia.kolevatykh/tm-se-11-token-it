package ru.kolevatykh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.Task;
import ru.kolevatykh.tm.wrapper.TaskWrapper;
import java.util.List;

public final class TaskListSortedByStartDateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-list-sorted-sd";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "tlssd";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tShow all tasks s sorted by start date.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = serviceLocator.getToken();
        System.out.println("[TASK LIST]");
        @Nullable final List<Task> taskList = serviceLocator.getTaskEndpoint().findTasksSortedByStartDate(token);
        if (taskList == null) {
            throw new Exception("[No tasks yet.]");
        }

        @NotNull final StringBuilder tasks = new StringBuilder();
        int i = 0;

        for (@NotNull final Task task : taskList) {
            TaskWrapper taskWrapper = new TaskWrapper(task);
            tasks
                    .append(++i)
                    .append(". ")
                    .append(taskWrapper.toString())
                    .append(System.lineSeparator());
        }

        @NotNull final String taskString = tasks.toString();
        System.out.println(taskString);
    }
}
