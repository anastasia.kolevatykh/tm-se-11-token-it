package ru.kolevatykh.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.endpoint.User;
import ru.kolevatykh.tm.util.ConsoleInputUtil;
import ru.kolevatykh.tm.util.PasswordHashUtil;

public final class UserAuthorizeCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-authorize";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ua";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sign in.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String currentToken = serviceLocator.getToken();
        if (currentToken != null) {
            throw new Exception("[You are authorized under login: "
                    + serviceLocator.getUserEndpoint().findUserById(currentToken).getLogin()
                    + "\nPlease LOGOUT first, in order to authorize under OTHER account.]");
        }

        System.out.println("[USER AUTHORIZATION]\nEnter your login: ");
        @NotNull final String login = ConsoleInputUtil.getConsoleInput();
        @Nullable final User user = serviceLocator.getUserEndpoint().findUserByLogin(login);
        if (user == null) {
            throw new Exception("[The login '" + login + "' does not exist. "
                    + "Please, retry or register.]");
        }

        System.out.println("Enter your password: ");
        @NotNull final String password = ConsoleInputUtil.getConsoleInput();
        if (password.isEmpty()) {
            throw new Exception("[The password can't be empty.]");
        }

        @NotNull final String pass = PasswordHashUtil.getPasswordHash(password);
        if (!user.getPasswordHash().equals(pass)) {
            throw new Exception("[Wrong password.]");
        }

        @NotNull final String token =
                serviceLocator.getTokenEndpoint().openTokenSession(login, pass);
        serviceLocator.setToken(token);
        System.out.println("[OK]");
    }
}
