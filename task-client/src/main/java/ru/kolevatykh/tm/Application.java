package ru.kolevatykh.tm;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.bootstrap.Bootstrap;

public final class Application {
    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
