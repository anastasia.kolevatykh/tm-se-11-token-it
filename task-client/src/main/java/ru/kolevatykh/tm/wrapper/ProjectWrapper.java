package ru.kolevatykh.tm.wrapper;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.endpoint.Project;

@Setter
@Getter
@NoArgsConstructor
public class ProjectWrapper extends Project {

    @NotNull
    private Project project;

    public ProjectWrapper(@NotNull final Project project) {
        this.project = project;
    }

    @Override
    public String toString() {
        return "userId: '" + project.getUserId() + '\'' +
                ", id: '" + project.getId() + '\'' +
                ", name: '" + project.getName() + '\'' +
                ", description: '" + project.getDescription() + '\'' +
                ", statusType: " + project.getStatusType() +
                ", startDate: " + project.getStartDate() +
                ", endDate: " + project.getEndDate();
    }
}
