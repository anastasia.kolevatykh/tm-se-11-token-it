package ru.kolevatykh.tm.wrapper;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.endpoint.Task;

@Setter
@Getter
@NoArgsConstructor
public class TaskWrapper extends Task {

    @NotNull
    private Task task;

    public TaskWrapper(@NotNull final Task task) {
        this.task = task;
    }

    @Override
    public String toString() {
        return "userId: '" + task.getUserId() + '\'' +
                ", projectId: '" + task.getProjectId() + '\'' +
                ", id: '" + task.getId() + '\'' +
                ", name: '" + task.getName() + '\'' +
                ", description: '" + task.getDescription() + '\'' +
                ", statusType: " + task.getStatusType() +
                ", startDate: " + task.getStartDate() +
                ", endDate: " + task.getEndDate();
    }
}
