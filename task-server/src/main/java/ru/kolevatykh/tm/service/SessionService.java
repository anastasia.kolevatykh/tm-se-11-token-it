package ru.kolevatykh.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ISessionService;
import ru.kolevatykh.tm.api.IUserService;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.api.ISessionRepository;

@Setter
@Getter
@NoArgsConstructor
public final class SessionService extends AbstractService<Session> implements ISessionService {

    @Nullable
    private IUserService userService;

    @Nullable
    private ISessionRepository sessionRepository;

    public SessionService(@NotNull final ISessionRepository sessionRepository,
                          @NotNull final IUserService userService) {
        this.setSessionRepository(sessionRepository);
        this.setUserService(userService);
    }

    @Override
    @Nullable
    public User getUser(@Nullable final Session session) {
        if (userService == null || sessionRepository == null || session == null) return null;
        @Nullable final User user = userService.findOneById(session.getUserId());
        return user;
    }

    @Override
    @Nullable
    public Session open(@Nullable final String login, @Nullable final String password) {
        if (userService == null || sessionRepository == null
                || login == null || login.isEmpty()
                || password == null || password.isEmpty()) return null;
        @Nullable final User user = userService.findOneByLogin(login);
        if (user == null) return null;
        if (!user.getPasswordHash().equals(password)) return null;
        @NotNull Session session = sessionRepository.open(user);
        return session;
    }

    @Override
    public void validate(@Nullable final Session session) throws Exception {
        if (sessionRepository == null || session == null) return;
        @NotNull final long timeOut = 2 * 60 * 1000L;
        if (System.currentTimeMillis() - session.getTimestamp() > timeOut) {
            remove(session.getId());
            throw new Exception("[The session is timed out.]");
        }
        @NotNull final String signature = Session.generateSignature(session);
        if (!signature.equals(session.getSignature()))
            throw new Exception("[Wrong session signature.]");
        if (sessionRepository.findOneById(session.getId()) == null)
            throw new Exception("[The session does not exist.]");
    }
}
