package ru.kolevatykh.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ITaskService;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.api.ITaskRepository;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskService extends AbstractProjectTaskService<Task> implements ITaskService {

    @Nullable
    private ITaskRepository taskRepository;

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        super(taskRepository);
        this.setTaskRepository(taskRepository);
    }

    @Override
    public void persist(@Nullable final String userId,
                        @Nullable final String projectId,
                        @Nullable final String name,
                        @Nullable final String description,
                        @Nullable final Date startDate,
                        @Nullable final Date endDate) {
        if (taskRepository == null
                || userId == null || userId.isEmpty()
                || name == null || name.isEmpty()) return;
        taskRepository.persist(userId, projectId, name, description, startDate, endDate);
    }

    @Override
    public void merge(@Nullable final String userId,
                      @Nullable final String projectId,
                      @Nullable final String id,
                      @Nullable final String name,
                      @Nullable final String description,
                      @Nullable final Date startDate,
                      @Nullable final Date endDate) {
        if (taskRepository == null
                || userId == null || userId.isEmpty()
                || id == null || id.isEmpty()
                || name == null || name.isEmpty()) return;
        taskRepository.merge(userId, projectId, id, name, description, startDate, endDate);
    }

    @Override
    public void removeTasksWithProjectId(@Nullable final String userId) {
        if (taskRepository == null || userId == null || userId.isEmpty()) return;
        taskRepository.removeTasksWithProjectId(userId);
    }

    @Override
    public void removeProjectTasks(@Nullable final String userId, @Nullable final String projectId) {
        if (taskRepository == null
                || projectId == null || projectId.isEmpty()
                || userId == null || userId.isEmpty()) return;
        taskRepository.removeProjectTasks(userId, projectId);
    }

    @Nullable
    @Override
    public final List<Task> findTasksByProjectId(@Nullable final String userId,
                                           @Nullable final String projectId) {
        if (taskRepository == null
                || projectId == null || projectId.isEmpty()
                || userId == null || userId.isEmpty()) return null;
        @NotNull final List<Task> taskList = taskRepository.findTasksByProjectId(userId, projectId);
        if (taskList.isEmpty()) return null;
        return taskList;
    }

    @Nullable
    @Override
    public final List<Task> findTasksWithoutProject(@Nullable final String userId) {
        if (taskRepository == null || userId == null || userId.isEmpty()) return null;
        @NotNull final List<Task> taskList = taskRepository.findTasksWithoutProject(userId);
        if (taskList.isEmpty()) return null;
        return taskList;
    }

    @Override
    public void assignToProject(@Nullable final String userId,
                                @Nullable final String id,
                                @Nullable final String projectId) {
        if (taskRepository == null || userId == null || userId.isEmpty()
                || id == null || id.isEmpty()
                || projectId == null || projectId.isEmpty()) return;
        taskRepository.assignToProject(userId, id, projectId);
    }
}
