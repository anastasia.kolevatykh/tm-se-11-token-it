package ru.kolevatykh.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.IRepository;
import ru.kolevatykh.tm.entity.AbstractEntity;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractService<T extends AbstractEntity> {

    @Nullable
    private IRepository<T> repository;

    AbstractService(@NotNull final IRepository<T> repository) {
        this.setRepository(repository);
    }

    @Nullable
    public final List<T> findAll() {
        if (repository == null) return null;
        @NotNull final List<T> entities = repository.findAll();
        if (entities.isEmpty()) return null;
        return entities;
    }

    @Nullable
    public final T findOneById(@Nullable final String id) {
        if (repository == null || id == null || id.isEmpty()) return null;
        return repository.findOneById(id);
    }

    public void remove(@Nullable final String id) {
        if (repository == null || id == null || id.isEmpty()) return;
        repository.remove(id);
    }

    public void removeAll() {
        if (repository == null) return;
        repository.removeAll();
    }
}
