package ru.kolevatykh.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.IUserService;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.api.IUserRepository;
import ru.kolevatykh.tm.enumerate.RoleType;

@Setter
@Getter
@NoArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {

    @Nullable
    private IUserRepository userRepository;

    @Nullable
    private User currentUser;

    public UserService(@NotNull final IUserRepository userRepository) {
        super(userRepository);
        this.setUserRepository(userRepository);
    }

    @Override
    public void persist(@Nullable final String login,
                        @Nullable final String passwordHash,
                        @Nullable final RoleType roleType,
                        @Nullable final Boolean isAuth) {
        if (userRepository == null
                || login == null || login.isEmpty()
                || passwordHash == null || passwordHash.isEmpty()
                || roleType == null || isAuth == null) return;
        userRepository.persist(login, passwordHash, roleType, isAuth);
    }

    @Override
    public void persist(@Nullable final User user) {
        if (userRepository == null || user == null) return;
        userRepository.persist(user);
    }

    @Override
    public void merge(@Nullable final String login,
                        @Nullable final String password,
                        @Nullable final RoleType roleType,
                        @Nullable final Boolean isAuth) {
        if (userRepository == null
                || login == null || login.isEmpty()
                || password == null || password.isEmpty()
                || roleType == null || isAuth == null) return;
        userRepository.merge(login, password, roleType, isAuth);
    }

    @Nullable
    @Override
    public final User findOneByLogin(@Nullable final String login) {
        if (userRepository == null || login == null || login.isEmpty()) return null;
        return userRepository.findOneByLogin(login);
    }

    public void createTestUsers() throws Exception {
//        if (userRepository == null) return;
//        DomainService domainService = new DomainService();
//        domainService.loadJacksonJson();
        //userRepository.persist("admin", PasswordHashUtil.getPasswordHash("pass1"), RoleType.ADMIN, false);
        //userRepository.persist("user", PasswordHashUtil.getPasswordHash("pass2"), RoleType.USER, false);
    }
}
