package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBException;

public interface IDomainService {
    void serialize() throws Exception;

    void deserialize() throws Exception;

    void saveJacksonJson() throws Exception;

    void loadJacksonJson() throws Exception;

    void saveJacksonXml() throws Exception;

    void loadJacksonXml() throws Exception;

    void marshalJaxbJson() throws JAXBException;

    void unmarshalJaxbJson() throws JAXBException;

    void marshalJaxbXml() throws JAXBException;

    void unmarshalJaxbXml() throws JAXBException;

    @Nullable ServiceLocator getServiceLocator();

    void setServiceLocator(@NotNull ServiceLocator serviceLocator);
}
