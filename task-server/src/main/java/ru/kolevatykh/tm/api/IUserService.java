package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.List;

public interface IUserService extends IService<User> {
    @Nullable User getCurrentUser();

    void setCurrentUser(@Nullable User user);

    @Nullable List<User> findAll();

    @Nullable User findOneById(@Nullable String id);

    @Nullable User findOneByLogin(@Nullable String login);

    void persist(@Nullable String login, @Nullable String password,
                 @Nullable RoleType roleType, @Nullable Boolean isAuth);

    void persist(@Nullable final User user);

    void merge(@Nullable String login, @Nullable String password,
               @Nullable RoleType roleType, @Nullable Boolean isAuth);

    void remove(@Nullable String id);

    void removeAll();

    void createTestUsers() throws Exception;
}
