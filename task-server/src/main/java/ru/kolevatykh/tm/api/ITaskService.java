package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IProjectTaskService<Task> {
    @Nullable List<Task> findAll();

    @Nullable List<Task> findAllByUserId(@Nullable String userId);

    @Nullable Task findOneById(@Nullable String userId, @Nullable String id);

    @Nullable Task findOneByName(@Nullable String userId, @Nullable String name);

    void persist(@Nullable final Task task);

    void persist(@Nullable String userId, @Nullable String projectId, @Nullable String name,
                 @Nullable String description, @Nullable Date startDate, @Nullable Date endDate);

    void merge(@Nullable String userId, @Nullable String projectId, @Nullable String id, @Nullable String name,
               @Nullable String description, @Nullable Date startDate, @Nullable Date endDate);

    void mergeStatus(@Nullable String userId, @Nullable String id, @Nullable String name);

    void remove(@Nullable String userId, @Nullable String id);

    void removeAll(@Nullable String userId);

    void removeTasksWithProjectId(@Nullable String userId);

    void removeProjectTasks(@Nullable String userId, @Nullable String projectId);

    @Nullable List<Task> findTasksByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable List<Task> findTasksWithoutProject(@Nullable String userId);

    void assignToProject(@Nullable String userId, @Nullable String id, @Nullable String projectId);

    @Nullable List<Task> findAllSortedByStartDate(@Nullable final String userId);

    @Nullable List<Task> findAllSortedByEndDate(@Nullable final String userId);

    @Nullable List<Task> findAllSortedByStatus(@Nullable final String userId);

    @Nullable List<Task> findAllBySearch(@Nullable final String userId, @Nullable final String search);
}
