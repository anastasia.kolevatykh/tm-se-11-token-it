package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.List;

public interface IUserRepository extends IRepository<User> {
    @NotNull List<User> findAll();

    @Nullable User findOneById(@NotNull String id);

    @Nullable User findOneByLogin(@NotNull String login);

    void persist(@NotNull String login, @NotNull String password,
                 @NotNull RoleType roleType, @NotNull Boolean isAuth);

    void persist(@NotNull final User user);

    void merge(@NotNull String login, @NotNull String password,
               @NotNull RoleType roleType, @NotNull Boolean isAuth);

    void remove(@NotNull String id);

    void removeAll();
}
