package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Project;

import java.util.Date;
import java.util.List;

public interface IProjectService extends IProjectTaskService<Project> {
    @Nullable List<Project> findAll();

    @Nullable List<Project> findAllByUserId(@Nullable String userId);

    @Nullable Project findOneById(@Nullable String userId, @Nullable String id);

    @Nullable Project findOneByName(@Nullable String userId, @Nullable String name);

    void persist(@Nullable final Project project);

    void persist(@Nullable String userId, @Nullable String name, @Nullable String description,
                 @Nullable Date startDate, @Nullable Date endDate);

    void merge(@Nullable String userId, @Nullable String id, @Nullable String name,
               @Nullable String description, @Nullable Date startDate, @Nullable Date endDate);

    void mergeStatus(@Nullable String userId, @Nullable String id, @Nullable String name);

    void remove(@Nullable String userId, @Nullable String id);

    void removeAll(@Nullable String userId);

    @Nullable List<Project> findAllSortedByStartDate(@Nullable final String userId);

    @Nullable List<Project> findAllSortedByEndDate(@Nullable final String userId);

    @Nullable List<Project> findAllSortedByStatus(@Nullable final String userId);
}