package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Project;

import java.util.Date;
import java.util.List;

public interface IProjectRepository extends IProjectTaskRepository<Project> {
    @NotNull List<Project> findAll();

    @NotNull List<Project> findAllByUserId(@NotNull String userId);

    @Nullable Project findOneById(@NotNull String userId, @NotNull String id);

    @Nullable Project findOneByName(@NotNull String userId, @NotNull String name);

    void persist(@NotNull final Project project);

    void persist(@NotNull String userId, @NotNull String name, @Nullable String description,
                 @Nullable Date startDate, @Nullable Date endDate);
    
    void merge(@NotNull String userId, @NotNull String id, @NotNull String name,
               @Nullable String description, @Nullable Date startDate, @Nullable Date endDate);

    void mergeStatus(@NotNull String userId, @NotNull String id, @NotNull String name);

    void remove(@NotNull String userId, @NotNull String id);

    void removeAll(@NotNull String userId);

    @NotNull List<Project> findAllSortedByStartDate(@NotNull final String userId);

    @NotNull List<Project> findAllSortedByEndDate(@NotNull final String userId);

    @NotNull List<Project> findAllSortedByStatus(@NotNull final String userId);
}
