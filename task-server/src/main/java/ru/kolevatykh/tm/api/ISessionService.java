package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.User;

public interface ISessionService extends IService<Session> {
    @Nullable User getUser(@Nullable Session session);

    @Nullable Session open(@Nullable String login, @Nullable String password);

    void validate(@Nullable Session session) throws Exception;

    @Nullable IUserService getUserService();

    @Nullable ISessionRepository getSessionRepository();

    void setUserService(@NotNull IUserService userService);

    void setSessionRepository(@NotNull ISessionRepository sessionRepository);
}
