package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.service.TokenService;

public interface ServiceLocator {
    @NotNull IUserService getUserService();

    @NotNull IProjectService getProjectService();

    @NotNull ITaskService getTaskService();

    @NotNull IDomainService getDomainService();

    @NotNull ISessionService getSessionService();

    @NotNull TokenService getTokenService();
}
