package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.User;

public interface ISessionRepository extends IRepository<Session> {
    @NotNull Session open(@NotNull User user);
}
