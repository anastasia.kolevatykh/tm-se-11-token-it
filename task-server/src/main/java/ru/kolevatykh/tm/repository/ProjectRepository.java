package ru.kolevatykh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.IProjectRepository;
import ru.kolevatykh.tm.entity.Project;
import java.util.Date;

public final class ProjectRepository extends AbstractProjectTaskRepository<Project> implements IProjectRepository {

    @Override
    public void persist(@NotNull final String userId,
                        @NotNull final String name,
                        @Nullable final String description,
                        @Nullable final Date startDate,
                        @Nullable final Date endDate) {
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        if (description != null) {
            project.setDescription(description);
        }
        if (startDate != null) {
            project.setStartDate(startDate);
        }
        if (endDate != null) {
            project.setEndDate(endDate);
        }
        map.put(project.getId(), project);
    }

    @Override
    public void merge(@NotNull final String userId,
                      @NotNull final String id,
                      @NotNull final String name,
                      @Nullable final String description,
                      @Nullable final Date startDate,
                      @Nullable final Date endDate) {
        @Nullable Project project = findOneById(userId, id);
        if (project == null) return;
        project.setName(name);
        if (description != null) {
            project.setDescription(description);
        }
        if (startDate != null) {
            project.setStartDate(startDate);
        }
        if (endDate != null) {
            project.setEndDate(endDate);
        }
        map.put(project.getId(), project);
    }
}
