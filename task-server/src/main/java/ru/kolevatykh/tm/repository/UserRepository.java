package ru.kolevatykh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.IUserRepository;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.*;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public void persist(@NotNull final String login,
                        @NotNull final String password,
                        @NotNull final RoleType roleType,
                        @NotNull final Boolean isAuth) {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRoleType(roleType);
        user.setAuth(isAuth);
        map.put(user.getId(), user);
    }

    @Override
    public void persist(@NotNull final User user) {
        map.put(user.getId(), user);
    }

    public void merge(@NotNull final String login,
                      @NotNull final String passwordHash,
                      @NotNull final RoleType roleType,
                      @NotNull final Boolean isAuth) {
        @Nullable final User user = findOneByLogin(login);
        if (user == null) return;
        user.setPasswordHash(passwordHash);
        user.setRoleType(roleType);
        user.setAuth(isAuth);
        map.put(user.getId(), user);
    }

    @Nullable
    @Override
    public final User findOneByLogin(@NotNull final String login) {
        for (@NotNull final Map.Entry<String, User> entry : map.entrySet()) {
            if (entry.getValue().getLogin().equals(login))
                return entry.getValue();
        }
        return null;
    }
}
