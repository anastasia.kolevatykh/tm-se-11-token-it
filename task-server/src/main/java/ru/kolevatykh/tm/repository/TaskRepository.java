package ru.kolevatykh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ITaskRepository;
import ru.kolevatykh.tm.entity.Task;

import java.util.*;

public final class TaskRepository extends AbstractProjectTaskRepository<Task> implements ITaskRepository {

    @Override
    public void persist(@NotNull final String userId,
                        @Nullable final String projectId,
                        @NotNull final String name,
                        @Nullable final String description,
                        @Nullable final Date startDate,
                        @Nullable final Date endDate) {
        Task task = new Task();
        task.setUserId(userId);
        task.setProjectId(projectId);
        task.setName(name);
        if (description != null) {
            task.setDescription(description);
        }
        if (startDate != null) {
            task.setStartDate(startDate);
        }
        if (endDate != null) {
            task.setEndDate(endDate);
        }
        map.put(task.getId(), task);
    }

    @Override
    public void merge(@NotNull final String userId,
                      @Nullable final String projectId,
                      @NotNull final String id,
                      @NotNull final String name,
                      @Nullable final String description,
                      @Nullable final Date startDate,
                      @Nullable final Date endDate) {
        @Nullable Task task = findOneById(userId, id);
        if (task == null) return;
        task.setProjectId(projectId);
        task.setName(name);
        if (description != null) {
            task.setDescription(description);
        }
        if (startDate != null) {
            task.setStartDate(startDate);
        }
        if (endDate != null) {
            task.setEndDate(endDate);
        }
        map.put(task.getId(), task);
    }

    @Override
    public void removeTasksWithProjectId(@NotNull final String userId) {
        for (Iterator<Map.Entry<String, Task>> it = map.entrySet().iterator(); it.hasNext(); ) {
            @NotNull final Map.Entry<String, Task> entry = it.next();
            @NotNull final Task task = entry.getValue();
            if (userId.equals(task.getUserId())
                    && task.getProjectId() != null)
                it.remove();
        }
    }

    @Override
    public void removeProjectTasks(@NotNull final String userId,
                                   @NotNull final String projectId) {
        for (Iterator<Map.Entry<String, Task>> it = map.entrySet().iterator(); it.hasNext(); ) {
            @NotNull final Map.Entry<String, Task> entry = it.next();
            @NotNull final Task task = entry.getValue();
            if (userId.equals(task.getUserId())
                    && projectId.equals(task.getProjectId()))
                it.remove();
        }
    }

    @NotNull
    @Override
    public final List<Task> findTasksByProjectId(@NotNull final String userId,
                                           @NotNull final String projectId) {
        @NotNull final List<Task> taskList = new ArrayList<>();
        for (@NotNull final Map.Entry<String, Task> entry : map.entrySet()) {
            if (userId.equals(entry.getValue().getUserId())
                    && projectId.equals(entry.getValue().getProjectId()))
                taskList.add(entry.getValue());
        }
        return taskList;
    }

    @NotNull
    @Override
    public final List<Task> findTasksWithoutProject(@NotNull final String userId) {
        @NotNull final List<Task> taskList = new ArrayList<>();
        for (@NotNull final Map.Entry<String, Task> entry : map.entrySet()) {
            if (userId.equals(entry.getValue().getUserId())
                    && entry.getValue().getProjectId() == null)
                taskList.add(entry.getValue());
        }
        return taskList;
    }

    @Override
    public void assignToProject(@NotNull final String userId,
                                @NotNull final String id,
                                @NotNull final String projectId) {
        if (userId.equals(map.get(id).getUserId()))
            map.get(id).setProjectId(projectId);
    }
}
