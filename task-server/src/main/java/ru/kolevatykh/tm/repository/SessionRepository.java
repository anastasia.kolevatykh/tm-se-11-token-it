package ru.kolevatykh.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.api.ISessionRepository;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.util.SignatureUtil;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    @NotNull
    public Session open(@NotNull final User user) {
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setRoleType(user.getRoleType());
        session.setSignature(SignatureUtil.sign(session, "salted", 13));
        map.put(session.getId(), session);
        return session;
    }
}
