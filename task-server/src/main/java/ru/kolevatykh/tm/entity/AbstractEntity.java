package ru.kolevatykh.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.UUID;

@Getter
@NoArgsConstructor(force = true)
public abstract class AbstractEntity implements Serializable {

    @XmlElement
    @NotNull
    protected final String id = UUID.randomUUID().toString();
}
