package ru.kolevatykh.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.util.SignatureUtil;

@Getter
@Setter
@NoArgsConstructor
public final class Token extends AbstractEntity implements Cloneable {

    @Nullable
    private Session session;

    @Nullable
    private String signature;

    public static String generateSignature(@NotNull final Token token) throws CloneNotSupportedException {
        @NotNull final Token tempToken = (Token) token.clone();
        tempToken.setSignature(null);
        return SignatureUtil.sign(tempToken, "salted", 13);
    }
}
