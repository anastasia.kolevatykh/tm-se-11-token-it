package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ISessionEndpoint {

    @NotNull String URL = "http://0.0.0.0:8080/SessionEndpoint?wsdl";

    @WebMethod
    void closeSession(
            @WebParam(name = "session") @Nullable final Session session
    );

    @WebMethod
    void closeAllSession();

    @Nullable
    @WebMethod
    List<Session> getListSession();

    @Nullable
    @WebMethod
    User getUser(
            @WebParam(name = "session") @Nullable Session session
    );

    @Nullable
    @WebMethod
    Session openSession(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    );
}
