package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ServiceLocator;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kolevatykh.tm.endpoint.ISessionEndpoint")
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint() {
    }

    public SessionEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void closeSession(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        if (session == null) return;
        serviceLocator.getSessionService().remove(session.getId());
    }

    @Override
    @WebMethod
    public void closeAllSession() {
        serviceLocator.getSessionService().removeAll();
    }

    @Override
    @Nullable
    @WebMethod
    public List<Session> getListSession() {
        return serviceLocator.getSessionService().findAll();
    }

    @Override
    @Nullable
    @WebMethod
    public User getUser(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @Nullable final User user = serviceLocator.getSessionService().getUser(session);
        return user;
    }

    @Override
    @Nullable
    @WebMethod
    public Session openSession(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) {
        @Nullable final Session session = serviceLocator.getSessionService().open(login, password);
        return session;
    }
}
