package ru.kolevatykh.tm.endpoint;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ServiceLocator;
import ru.kolevatykh.tm.entity.Token;
import ru.kolevatykh.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Setter
@Getter
@WebService(endpointInterface = "ru.kolevatykh.tm.endpoint.ITokenEndpoint")
public class TokenEndpoint extends AbstractEndpoint implements ITokenEndpoint {

    public TokenEndpoint() {
    }

    public TokenEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void closeTokenSession(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return;
        serviceLocator.getTokenService().closeTokenSession(token);
    }

    @Override
    @WebMethod
    public void closeAllTokenSession(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return;
        serviceLocator.getTokenService().removeAll();
    }

    @Override
    @Nullable
    @WebMethod
    public List<Token> getListTokenSession(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return null;
        return serviceLocator.getTokenService().findAll();
    }


    @Override
    @Nullable
    @WebMethod
    public User getUserByToken(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return null;
        @Nullable final User user = serviceLocator.getTokenService().getUser(token);
        return user;
    }

    @Override
    @Nullable
    @WebMethod
    public String openTokenSession(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final String tokenString = serviceLocator.getTokenService().open(login, password);
        return tokenString;
    }
}
