package ru.kolevatykh.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.ServiceLocator;
import ru.kolevatykh.tm.entity.Session;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.entity.Token;
import ru.kolevatykh.tm.util.DateFormatterUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kolevatykh.tm.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint() {
    }

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @WebMethod
    public final List<Task> findAllTasksByUserId(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return null;
        @Nullable final Session session = token.getSession();
        if (session == null) return null;
        return serviceLocator.getTaskService().findAllByUserId(session.getUserId());
    }

    @Nullable
    @WebMethod
    public final Task findTaskById(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return null;
        @Nullable final Session session = token.getSession();
        if (session == null) return null;
        return serviceLocator.getTaskService().findOneById(session.getUserId(), id);
    }

    @Override
    @Nullable
    @WebMethod
    public final Task findTaskByName(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "name") @Nullable final String name
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return null;
        @Nullable final Session session = token.getSession();
        if (session == null) return null;
        return serviceLocator.getTaskService().findOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void persistTask(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "endDate") @Nullable final String endDate
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return;
        @Nullable final Session session = token.getSession();
        if (session == null) return;
        serviceLocator.getTaskService().persist(session.getUserId(), projectId, name, description,
               DateFormatterUtil.parseDate(startDate), DateFormatterUtil.parseDate(endDate));
    }

    @Override
    @WebMethod
    public void mergeTask(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description,
            @WebParam(name = "startDate") @Nullable final String startDate,
            @WebParam(name = "endDate") @Nullable final String endDate
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return;
        @Nullable final Session session = token.getSession();
        if (session == null) return;
        serviceLocator.getTaskService().merge(session.getUserId(), projectId, id, name, description,
                DateFormatterUtil.parseDate(startDate), DateFormatterUtil.parseDate(endDate));
    }

    @Override
    @WebMethod
    public void mergeTaskStatus(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "status") @Nullable final String status
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return;
        @Nullable final Session session = token.getSession();
        if (session == null) return;
        serviceLocator.getTaskService().mergeStatus(session.getUserId(), id, status);
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return;
        @Nullable final Session session = token.getSession();
        if (session == null) return;
        serviceLocator.getTaskService().remove(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeAllTasks(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return;
        @Nullable final Session session = token.getSession();
        if (session == null) return;
        serviceLocator.getTaskService().removeAll(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Task> findTasksSortedByStartDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return null;
        @Nullable final Session session = token.getSession();
        if (session == null) return null;
        return serviceLocator.getTaskService().findAllSortedByStartDate(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Task> findTasksSortedByEndDate(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return null;
        @Nullable final Session session = token.getSession();
        if (session == null) return null;
        return serviceLocator.getTaskService().findAllSortedByEndDate(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Task> findTasksSortedByStatus(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return null;
        @Nullable final Session session = token.getSession();
        if (session == null) return null;
        return serviceLocator.getTaskService().findAllSortedByStatus(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Task> findTasksBySearch(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "search") @Nullable final String search
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return null;
        @Nullable final Session session = token.getSession();
        if (session == null) return null;
        return serviceLocator.getTaskService().findAllBySearch(session.getUserId(), search);
    }

    @Override
    @WebMethod
    public void removeTasksWithProjectId(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return;
        @Nullable final Session session = token.getSession();
        if (session == null) return;
        serviceLocator.getTaskService().removeTasksWithProjectId(session.getUserId());
    }

    @Override
    @WebMethod
    public void removeProjectTasks(
            @WebParam(name = "token") @Nullable final String tokenString,

            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return;
        @Nullable final Session session = token.getSession();
        if (session == null) return;
        serviceLocator.getTaskService().removeProjectTasks(session.getUserId(), projectId);
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Task> findTasksByProjectId(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return null;
        @Nullable final Session session = token.getSession();
        if (session == null) return null;
        return serviceLocator.getTaskService().findTasksByProjectId(session.getUserId(), projectId);
    }

    @Override
    @Nullable
    @WebMethod
    public final List<Task> findTasksWithoutProject(
            @WebParam(name = "token") @Nullable final String tokenString
    ) throws Exception {
        if (serviceLocator == null) return null;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return null;
        @Nullable final Session session = token.getSession();
        if (session == null) return null;
        return serviceLocator.getTaskService().findTasksWithoutProject(session.getUserId());
    }

    @Override
    @WebMethod
    public void assignToProject(
            @WebParam(name = "token") @Nullable final String tokenString,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        if (serviceLocator == null) return;
        @Nullable final Token token = serviceLocator.getTokenService().validate(tokenString);
        if (token == null) return;
        @Nullable final Session session = token.getSession();
        if (session == null) return;
        serviceLocator.getTaskService().assignToProject(session.getUserId(), id, projectId);
    }
}
